<?php

$forum = mysql_fetch_object(mysql_query('SELECT `id`, `access`, `name` FROM `forum` WHERE `id` = '.intval($_GET['forum'])));
$razdel = mysql_fetch_object(mysql_query('SELECT `id`, `id_forum`, `name` FROM `forum_razdels` WHERE `id_forum` = '.$forum->id.' AND `id` = '.intval($_GET['razdel'])));
$theme = mysql_fetch_object(mysql_query('SELECT `id`, `id_razdel`, `name` FROM `forum_themes` WHERE `id_razdel` = '.$razdel->id.' AND `id` = '.intval($_GET['theme'])));

if (!$theme || !$razdel || !$forum || ($forum->access == 1 && $user['group_access'] < 8) || ($forum->access == 2 && $user['group_access'] < 3)) {
    header('Location: '.FORUM);
    exit;
} else {
    $set['title'] = 'Файлы темы - '.output_text($theme->name, 1, 1, 0, 0, 0);
    include_once '../sys/inc/thead.php';
    title().aut();

    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?= FORUM ?>'>Форум</a> / <a href = '<?= FORUM.'/'.$forum->id ?>/'><?= output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?= output_text($razdel->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?= output_text($theme->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?
    $k_post = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_post_files` WHERE `id_theme` = '.$theme->id), 0);
    $k_page = k_page($k_post, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str']*$page-$set['p_str'];
    if ($k_post == 0) {
        ?>
        <div class = 'err'>В этой теме нет файлов.</div>
        <?
    }
    ?>
    <table class = 'post' cellspacing = '0' cellpadding = '0'>
        <?
        $files = mysql_query('SELECT * FROM `forum_post_files` WHERE `id_theme` = '.$theme->id.' ORDER BY `id` ASC LIMIT '.$start.', '.$set['p_str']);
        while ($file = mysql_fetch_object($files)) {
            $ras = strtolower(preg_replace('#^.*\.#', NULL, $file->name));
            if ($ras == 'jpg' || $ras == 'jpeg' || $ras == 'gif' || $ras == 'png' || $ras == 'bmp' || $ras == 'ico') {
                $icon = FORUM.'/files/'.$file->name;
            } elseif ($ras == '3gp' || $ras == 'mp4' || $ras == 'avi' || $ras == 'mpeg' || $ras == 'flv' || $ras == 'wmv' || $ras == 'mkv') {
                $icon = FORUM.'/icons/files/video.png';
            } elseif ($ras == 'docx' || $ras == 'doc' || $ras == 'docm' || $ras == 'dotx' || $ras == 'dot' || $ras == 'dotm') {
                $icon = FORUM.'/icons/files/word.png';
            } elseif ($ras == 'mp1' || $ras == 'mp2' || $ras == 'mp3' || $ras == 'wav' || $ras == 'aif' || $ras == 'ape' || $ras == 'flac' || $ras == 'ogg' || $ras == 'asf' || $ras == 'wma') {
                $icon = FORUM.'/icons/files/music.png';
            } elseif ($ras == 'zip' || $ras == 'rar' || $ras == 'tar' || $ras == '7-zip' || $ras == 'gzip' || $ras == 'jar' || $ras == 'jad' || $ras == 'war' || $ras == 'xar') {
                $icon = FORUM.'/icons/files/archive.png';
            } elseif ($ras == 'txt' || $ras == 'xml') {
                $icon = FORUM.'/icons/files/txt.png';
            } elseif ($ras == 'pdf') {
                $icon = FORUM.'/icons/files/pdf.png';
            } elseif ($ras == 'psd') {
                 $icon = FORUM.'/icons/files/psd.png';
            } else {
                $icon = FORUM.'/icons/files/file.png';
            }
            ?>
            <tr>
                <td class = 'p_t' style = 'width: 50px; border-right: none'>
                    <a href = '<?= $icon ?>'><img src = '<?= $icon ?>' alt = '' style = 'width: 50px; height: 50px' /></a>
                </td>
                <td class = 'p_t' style = 'border-left: none'>
                    <?= output_text($file->real_name, 1, 1, 0, 0, 0) ?><br />
                    Размер файла: <?= size_file($file->size) ?>
                </td>
            </tr>
            <?
        }
        ?>
    </table>
    <?
    if ($k_page > 1) {
        str(FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/files/', $k_page, $page);
    }
    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?= FORUM ?>'>Форум</a> / <a href = '<?= FORUM.'/'.$forum->id ?>/'><?= output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?= output_text($razdel->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?= output_text($theme->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?
}

?>